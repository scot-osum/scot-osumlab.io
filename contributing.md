# Contributing

Anybody is free to make contribution Merge Requests on any aspect of the site.

## Meetups data file

The [`_data/meetups.yml`](_data/meetups.yml) file is the main file that we'd like people to add to!

You can simply log in to Gitlab.com, go to the link lited above, go to the file and click "Web IDE", then create your merge request.

If you are unfamiliar with Git and merge requests, simply get in touch with us via the [Meetup.com group](https://www.meetup.com/Scotland-Open-Source-Users-Meetup/) and detail:

* the name of your meetup
* your meetup's URL (can be custom website)
* the town it is hosted in

For the meetup to be relevant to our list, it should have the following properties:

* It is focused on discussing Free Software and Open Source software, and promoting Openness and Freedom of software
* It is organised in a physical location with face-to-face events
* It is based in Scotland (we're trying to not bite off more than we can chew - though if you feel the scope should grow, let us know!)
* It intends to be recurring / plans to have multiple events in the future (must be currently active)
