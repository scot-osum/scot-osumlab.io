---
layout: home
---
{% assign sorted_meetups = site.data.meetups | sort: 'town' %}

# Scotland Open Source Users Meetups

This is a list of meetup groups and events where the emphasis is in promoting Free and Open Source Software to their local communities.

Anybody is free to add to this list - the more we know about them, the better we can share!

## Affiliated meetups

Meetups affiliated with this site abide by our [Code of Conduct](/coc/). The organisers for each can be found there.

Town | Meetup
-----|-------
{% for meetup in sorted_meetups %}{% if meetup.affiliated %}{{ meetup.town }} | [{{ meetup.name }}]({{meetup.url}})
{% endif %}{% endfor %}

[![Meetup.com logo](/assets/meetup-logo-m-swarm.svg) Our events are on Meetup.com]({{site.data.osumurl.meetups}})


## Additional meetups

Meetups not affiliated with this site have their own rules. This site does not exercise any influence over their operation -- please see their respective web sites for details.

Town | Meetup
-----|-------
{% for meetup in sorted_meetups %}{% unless meetup.affiliated %}{{ meetup.town }} | [{{ meetup.name }}]({{meetup.url}})
{% endunless %}{% endfor %}

## Yours too!

We'd love to list your town's open source meetups - to do so you can

* get in touch with details of your meetup (see [notes for relevant meetups](https://gitlab.com/scot-osum/scot-osum.gitlab.io/blob/master/contributing.md#meetups-data-file))
    * through [Meetup.com]({{ site.data.osumurl.meetups }})
    * or [with the organisers directly](/coc/#organisers)
* or [edit the list on Gitlab]({{site.data.osumurl.contribute}}) and make a Pull Request!

<hr />
