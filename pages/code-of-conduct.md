---
layout: page
permalink: /coc/
title: CoC
---

This code of conduct applies to meetups directly organised by Scotland OSUM (this site).

# Scotland OSUM Code of Conduct

Scotland Open Source Users Meetups (Scotland OSUM) is dedicated to providing a harassment-free community for everyone, regardless of sex, gender identity or expression, sexual orientation, disability, physical appearance, age, body size, race, nationality, or religious beliefs. We do not tolerate harassment of community members in any form. Participants violating these rules may be sanctioned or expelled from the community at the discretion of the Scotland OSUM organisers.

Harassment includes offensive verbal or written comments related to sex, gender identity or expression, sexual orientation, disability, physical appearance, age, body size, race, nationality, or religious beliefs; deliberate intimidation, threats, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention. Sexual language and imagery is not appropriate for any Scotland OSUM event or communication channel. Community members asked to stop any harassing behaviour are **expected to comply immediately**.

If a community member engages in harassing behaviour, the Scotland OSUM organisers may take any action they deem appropriate, including warning the offender, asking the offender to leave the discussion or event, expulsion from the Scotland OSUM community, and/or removal from an event, without refund in cases of paid-for events.

All Scotland OSUM community members, organisers, guests and sponsors will be expected to adhere to this code of conduct when engaged with activities and events related to Scotland OSUM, including in online communications, in-person communication, presentation materials and in any materials and designs brought to an Scotland OSUM event.

**If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact an Scotland OSUM organiser immediately, or as soon as you feel comfortable doing so.**

<hr />
<p id="organisers" />

The Scotland OSUM meeting organisers are listed below. Please contact the appropriate organiser for your area in case of need. If you have a concern with one of the organisers, please contact the other organisers to bring the matter to their attention.

{% assign sorted_meetups = site.data.meetups | sort: 'town' %}

Town | OSUM | Organiser
---|---|---
{% for meetup in sorted_meetups %}{% if meetup.affiliated %}{{ meetup.town }} | [{{ meetup.name }}]({{meetup.url}}) | {{ meetup.affiliated }}
{% endif %}{% endfor %}

If you have questions or feedback about this Code of Conduct please contact the organisers.

<hr />

This Code of Conduct, like its inspiration from the [Portland Python User Group][PDX] and [Geek Feminism Anti-Harassment][geek-feminism-AH] page, is licensed under the [Creative Commons Zero][cc-zero] license.

Should a need arise to enforce this CoC, a reference approach can be found on [the PyLadies website][pyladies-enforce].



[PDX]: https://www.meetup.com/pdxpython/pages/12061872/Code_of_Conduct/
[geek-feminism-AH]: https://geekfeminism.wikia.org/wiki/Conference_anti-harassment/Policy
[cc-zero]: https://creativecommons.org/publicdomain/zero/1.0/
[pyladies-enforce]: https://reshamas.github.io/managing-our-code-of-conduct/#addressing-violations
