---
layout: page
permalink: /podcast/
title: Podcast
---

# What's the Scotland Open Source podcast?
The SOSUM podcast "Scotland Open Source" aims to provide a relaxed but informative one-to-one interviews and conversations with Scottish open source developers, advocates and companies to help promote themselves, their projects and generally discuss the importance and impact of open source in the Scottish tech industry.

# Episodes
<div id="archives">
    {% for post in site.categories.podcasts %}
    <article class="archive-item">
      <h4><a href="{{ site.baseurl }}{{ post.url }}">{{post.title}}</a></h4>
      <p><em>{{ post.podcast_subtitle }}</em></p>
    </article>
    {% endfor %}
</div>

# Aspirations:
* Increase the personal or project awareness of Scottish open source advocates to the UK development community and companies. Thus directing and encouraging new contributors, users and patrons to these projects.
* Support and grow the network of open source advocates within Scotland by using the SOSUM platform/name to develop productive and beneficial personal and professional connections.
* Highlight and discuss aspects of working or volunteering in open source projects from a personal or business perceptive in the Scottish tech industry
* Identify and debate whether open source principles has impacted or complimented Scottish tech culture and how it could in the future.

# Topics:
For season 1 the spotlight topic of each episode will be of the guest’s project or work including their day-to-day tasks, thoughts and motivation behind their contributions and any aspects of open source they feel passionate about. As the format of the episode is informal, conversational topics may progress naturally. 
SOSUM provides [generic topics][podcastTopics] that the guest can choose to explore in relevance to their work or involvement prior to the recording session.

If you have any questions or wish to explore certain topics with our guests please raise the suggestion [here on the topic thread][podcastTopics]!

# Guest/Project Nominations:
If you would like to nominate a guest (self-nominations are weclomed) to participate in a SOSUM podcast episode to talk about their project(s), contributions or generally things involving open source and how it impacts Scottish tech industry please submit their details via the online form and we'll be in touch!

[SOSUM guest nomination submission form][SOSUMGuestForm]

# Code of Conduct:
SOSUM Code of Conduct (CoC) is stated [here][CoC] and expect all attendees and presenters to behave according to professional standards and abide by the SOSUM CoC.
Any guest or listener feels that an action(s) has violated the CoC, or any other concerns, please contact an Scotland OSUM organiser immediately, or as soon as you feel comfortable doing so.

# Raising Awareness:
* Multiple tweets promoting each episode and direct linking to the guest’s selection of links suitable to the target audience/platform i.e. twitter handle, project’s page/repo etc.
* Episode’s themselves will be hosted on a third-party platform (due to hosting limitations) that supports multiple major podcast platforms e.g. ApplePodcasts, TuneIn etc.
* Each episode’s notes, resources and transcript will be hosted on [SOSUM’s GitLab repo][SOSUMrepo]
* Supplement material will be requested: guest’s avatar or photo, guest’s bio in FOSS, and links to profile/website etc.

# Format:
* Each episode aims to be between 20 – 50 mins (depending on content)
* Episode candace initially will be monthly but may become fortnightly depending on the backlog of episodes. Season length ( 6-12 episodes ) also depends on the number of episodes recorded.



[SOSUMrepo]: https://gitlab.com/scot-osum/scot-osum.gitlab.io/
[CoC]: https://scot-osum.gitlab.io/coc/
[SOSUMGuestForm]: https://sosum.limequery.com/232171
[podcastTopics]: https://gitlab.com/scot-osum/scot-osum.gitlab.io/-/issues/1
