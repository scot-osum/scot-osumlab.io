---
layout: post
title: Ep05 - Open Source in Biomedical Engineering at the University of Glasgow with Dr Bernd Porr
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcasts/2020/12/07/sosum-podcast-s01-ep05.html
podcast_title: Ep05 - Open Source in Biomedical Engineering at the University of Glasgow with Dr Bernd Porr
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/v2/episodes/42363603/download.mp3"
podcast_guid: 42363603
podcast_length: 6451916
podcast_author: Scotland Open Source
podcast_keywords: technology, scotland, opensource, education, software
podcast_duration: "48:28"
podcast_description: We're joined by Dr Bernd Porr from the Engineering school of Glasgow University to discuss how and why he has introduced open source to his Biomedical Engineering students. We talk about the importance of teaching open source, especially those not in computer science studies, as well as what would encourage universities to implement more open source education. We also delve into how open source has benefited the neuroscience community with The Human Brain Project and Dr Porr's own contributions of the mapping of the limbic systems – plus I learn a few things about the reward system in Rats. Lastly talk about the University's support of open source by other departments, what advice he would give to contributors and Dr Porr's (many amazing) personal projects.
podcast_subtitle: We're joined by Dr Bernd Porr from the Engineering school of Glasgow University to discuss how and why he has introduced open source to his Biomedical Engineering students....
podcast_summary: We're joined by Dr Bernd Porr from the Engineering school of Glasgow University to discuss how and why he has introduced open source to his Biomedical Engineering students. We talk about the importance of teaching open source, especially those not in computer science studies, as well as what would encourage universities to implement more open source education. We also delve into how open source has benefited the neuroscience community with The Human Brain Project and Dr Porr's own contributions of the mapping of the limbic systems – plus I learn a few things about the reward system in Rats. Lastly talk about the University's support of open source by other departments, what advice he would give to contributors and Dr Porr's (many amazing) personal projects.
---

We're joined by Dr Bernd Porr from the Engineering school of Glasgow University to discuss how and why he has introduced open source to his Biomedical Engineering students. We talk about the importance of teaching open source, especially those not in computer science studies, as well as what would encourage universities to implement more open source education. We also delve into how open source has benefited the neuroscience community with The Human Brain Project and Dr Porr's own contributions of the mapping of the limbic systems – plus I learn a few things about the reward system in Rats. Lastly talk about the University's support of open source by other departments, what advice he would give to contributors and Dr Porr's (many amazing) personal projects.

...

<a class="spreaker-player" href="https://www.spreaker.com/episode/42363603" data-resource="episode_id=42363603" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep05 - Open Source in Biomedical Engineering at the University of Glasgow with Dr Bernd Porr" on Spreaker.</a>


## Shownotes

* 0:00 - Starting off things differently
* 0:52 - Introduction
* 1:56 - How did you start your journey into open source?
* 3:43 - Reminising CDs in magazines?
* 4:36 - Open Source as part of the Real Time Embedded Programming module @ Glasgow University Engineering
* 7:40 - Encourging (forced) students to code on supplied Raspberry Pis and publish it
* 8:58 - Did the students hear about open source before the course considering they may have came from different disciplines other than computer sciene.
* 11:06 - Teaching students the culture of open source is important
* 11:28 - Do you get a chance to teach the students the culture, licensing and aspects of participation?
* 12:59 - What projects were you fond of that the students made open source?
* 13:24 - GhostChess (student project)
* 14:14 - Hand gesture recognition (student project)
* 15:32 - Do any of the student go back to their projects in the future?
* 16:36 - How can we encourage students to continue in open source?
* 17:05 - Need more government policy and companies that include and promote open source
* 18:04 - Any difficulties getting this open source module into the course? Is the I.T. department supportive of this introduction?
* 18:50 - Academia love for MatLab 
* 20:11 - Do you think open source would benefit from other backgrounds of studies?
* 21:34 - Being open source helps find bugs and provides a level of creditiabily to the research outcomes veruses closed source software
* 22:18 - The Human Brain Project and open source
* 24:00 - Neurobotics platform to link up brains to accurately simulate environments
* 25:02 - A previous student presenting using this at the Fens (Federation of European Neuroscience Societies) Forum 2020 (held in Glasgow)
* 25:13 - Is there a percentage of how much of the brain has been mapped and how much to go?
* 27:11 - One of your contributions is the mapping of the limbic system. What is this system?
* 27:37 - Our limbic system is almost identical to a rat's
* 28:31 - Is there more to be mapped or do you intend to map others?
* 28:40 - The limbic system is extremely complex and current research is on how depression is involved with the firing of different hormones
* 30:22 - The project contains open sourced brain models as well as the tools to use them like the brain simulator
* 31:06 - Lowering the barrier to access these tools
* 32:18 - Why do you feel we should put FOSS in a promiment position infront of students aswell as universities?
* 34:31 - We need a course dedicated to open source to highlight its importance within the industry
* 34:05 - Always a slight disconnect between education and the industry but trying to bridge this gap by inviting those to the classes
* 35:44 - What do you feel would have the most impact of advoacing open source within education - the inclusion of the tech itself or the participation in the coursework?
* 38:00 - How closely do you work with the University's I.T. department with introducing open source?
* 38:49 - They introduced a second seperate network to support the Raspberry Pis
* 40:42 - As a long time contributor to FOSS what advice would you give to contributors to educate themselves on?
* 42:36 - Do you have any other projects you are working on?
* 43:20 - Filming - Vagabondo "How do EU citizens living in the UK feel after the Brexit vote?"
* 44:43 - How can people reach you?
* 45:34 - Any other events you are attending? FENS Glasgow 2020 & New York Film Festival
* 46:39 - Active in Glasgow Loves EU group
* 47:02 - Thank you

## Resources

* [Website: https://www.berndporr.me.uk/](https://www.berndporr.me.uk/)
* [Twitter: @berndporr](https://twitter.com/berndporr)
* [GitHub: https://github.com/berndporr](https://github.com/berndporr)
* [The Human Brain Project](https://www.humanbrainproject.eu/en/)
* [The Human Brain Project (GitHub)](https://github.com/HumanBrainProject/ )
* [Dr Bernd Porr Staff Profile](https://www.gla.ac.uk/schools/engineering/staff/berndporr/)
* [GhostChess: Hackaday](https://hackaday.com/2018/04/19/play-chess-against-a-ghost/)
* [Glasgow Neuro GitHub](https://github.com/glasgowneuro)
* [Glasgow Neuro](http://www.glasgowneuro.tech/)
* [Tinnitus Tailor](http://www.tinnitustailor.tech/)
* [Attys: open source high precision bluetooth data acquisition device](https://www.attys.tech/)
* [Eigen Productions](https://www.eigenproductions.co.uk/)
* [Fens 2020 Glasgow](https://forum2020.fens.org/destination-glasgow/)


## Transcript

* [Transcript (Coming Soon)]()

## Contribute

If you see anything to adjust, spelling or context, please feel free to contribute on the [Scotland Open Source Users Meetups's Repo](https://gitlab.com/scot-osum/scot-osum.gitlab.io/)

