---
layout: post
title: Session notes from Funding Open Source Projects
author: Tai Kedzierski
---

# Scotland OSUM - Funding Open Source Projects : a Guided Conversation

Thank you to all who attended our inaugural event! We were able to have an informative session on the topic of the methods and effects around funding open source projects, exchanging opinions and stories. The following is a summary of the conclusions, some notes on a few of the points raised with some post-event checking up, and underneath are the questions and answers provided during the Guided Conversation session.

## Summary

A few takeaways from the session stand out:

Firstly, it is not a given that all projects want to be funded, either via crowd-sourcing or company backing - many projects are maintained as a side-item for fun or originated as an exercise. If they become popular and even a key technology, it is by accident. Maintainers of such projects may want to keep income separate from their projects. There are plenty of ways to keep these projects going, including donating time, and assistance, and community management.

For those that want to be funded, some simply do not feel they have a case to present to ask for funding. There is a perception around raising funds in which maintainers may feel they have to "sell" the project forward, in the same way that a company does - the OpenSSL or Slackware projects received only a modest amount of financial and code-contribution help when it was discovered their maintainers were struggling to keep up. The concept of "funding" or "donation" feels like a pressure on future activities, rather than a payment for work already done, and making forward commitments due to money can be scary.

Funding is also perceived as a double-edged blade, which might allow maintainers to move the project from being a hobby to being a full-time job and be paid for their work, but at the same time money can make a project become a burden, and tie maintainers to expectations and outside influence, leading both to a loss of control of a project, or vocal community backlashes. Setting up an entity that dissociates the maintainers personally from the project requires administrative time and skills, as does managing what sources to accept income from, or to whom to apply to, as well as managing the transparency of expenses carefully. When projects start down this path, they become more like businesses, and become perceived as such, with the community/public expectations that come with it. At the end of the day, most open source developers are just developers - not business people, and accepting money could force this change upon them.

As an addendum from me, Tai, I did try to look up a couple of the points I felt needed checking up on:

1. OpenSSL received some donations, but [not nearly as much][openssl-poor-funding] as represents their relevance to the industry. Both the OpenSSL Software Foundation president Steve Marquess and Theo de Raadt of OpenBSD highlighted the problem of large corporations taking their technology, turning a profit, and giving nothing back
2. As a result of Heartbleed, the Linux Foundation initiated the [Core Infrastructure Initiative][lf-cii], but it seems it has [put out little since 2018][cii-news]
3. There are a number of grant funds that can be applied to if a project wishes to - the [Linux Foundation once published a list][lf-grants] to this effect
4. The Slackware maintainer, Patrick Volkerdink, [set up a Patreon][slackware-patreon] ([verified through his forum account][slackware-patreon-forum]) which is currently (August 2019) at $900 per month. Not modern salary level, but hopefully he can keep the lights on
5. A previous attempt at [providing a managed funding solution][codesponsor] was made, but eventually shut down when Github cut off their source of clicks
    * a number of reasons why developers tend not to want to manage funding are covered in this article, of which the requirement to step away from code as a result
6. Another article on [opensource.guide][opguide] highlights the challenges of getting funded, and lists some options, with success examples.
    * For every success though, there are tens of thousands of failed attempts - it is still recognised as "rare" and "extremely difficult"

One last, very underrated way to help ensure a project survives which was highlighted as a last point to the last question: *seek to be kind to the maintainers*. Clément Lefebvre of Linux Mint [posted an article in April 2019][mint-clem] linking to an older post by [George Stavracas, maintainer of GNOME Calendar][george-maintainer] about the negative aspects of being a Free Software maintainer. It boils eventually down to this: "*Unfortunately, being a free software maintainer may have a high price to your psychological and emotional health.*" The problem of user outrage can be amplified by funding, contributing perhaps to some fear of it. Contributing is not always funding, or code, or troubleshooting. Contributing can also be thanking, and making the thanks louder than the ire.

# Questions

## 1. What ways are there to fund FOSS projects and their teams?

* Donations - voluntary financial contributions
* [Bounty Source][bountysource] - a feature or bug gets registered, and money pledged against it. As more users pledge to the specific bug/feature, it raises in priority. Developers of that feature receive the funds
* [Grants][lf-grants] - the EU (see Horizon 2020 initiative), the UK government and other bodies provide grants upon application
* Hiring developers - for example, hiring the core developer of a project into a company, and either building on their work, or providing time for them to maintain the OSS project whilst at the company
* [Github Sponsors][githubsponsors]
* [Open Collective][opencollective] - a platform for collecting funds and expensing, and providing a Fiscal Host for a group of people, so as to avoid having to form a legal entity
* For web apps, providing a hosted version with a pay-for model, encapsulated in a company
* Selling consultancy and support as a business

## 2. Whose responsibility is it to maintain the sustainability of a FOSS project? Maintainers, contributors or consumers?

Maintainers should ensure the success of the project - it forms the basis as to "why" the project should be supported, and the greater its deployment/usage popularity, the better its chances at being funded. Contributors to the project themselves have effectively paid with their time, so it feels that the burden of sustainability keeping is not theirs to ensure. Consumers of the project - be they end users or companies who make use of the project - should proactively play their part in ensuring the project can keep the lights on.

(edit note:

* developers [don't usually want to change roles][codesponsor] to business maintainer
* even when they do, large companies often [don't behave responsibly][openssl-poor-funding]

)

## 3. What benefits would an FOSS project get from receiving funding?

Benefits perceived include

* relaxing the operational pressure, from a financial point of view (e.g. cost of running a website, hosted storage, for some like Linux distros this is monthly bandwidth or maintaining a Content Distribution Network subscription, etc)
* Potential to be able to scale a project if there is enough funding to start hiring developers
* Allowing disparate teams of developers to meet in-person

A concern for some is the spending of funds "appropriately;" it was pointed out that generally if the project mis-spends the funds, future donations can be negatively impacted, so this self-regulates for the most part.

There were also concerns raised about the effects of receiving funding - some projects likely choose not to be funded deliberately.

* When funded, there is a higher pressure to deliver on-demand, which is undesirable for small or boutique projects that did not intend critical success
* It potentially turns what was meant to be a fun project into a job, often alongside a full time job
* Monthly funding is preferable to one-time cash donations, for reasons of predictability


## 4. What perception do people have of financially donating to a FOSS project?

The terms "donation" was quickly called out as being problematic - it gives the impression of being a charity. This can set false expectations, both for developers and for funders. It cultivates the idea of giving out of pity, rather than recognising the service rendered by maintaining important software.

Some developers include a small statement in their Read-Me files asking for "beer money" tips, for small projects sometimes this feels appropriate when little is expected.

There is also a perception that each funder has a limited "donations budget" so figuring out who "deserves" funding more can also be a thorny issue.

## 5. What barriers would prevent projects from receiving donations or funding?

The first barrier to seeking funding is the previously-raised concern about being "tied to obligations" when funding is involved; the perceived pressure to "deliver" is too much if the project didn't begin as a means for a living.

There are also issues relating to registering a legal entity, sorting out tax and other legal matters, and maintaining the legal status for being able to receive the funds.

Finally in a group of peers, this forces the designation of a "lead person" who makes decisions on behalf of the group - and to decide how to spend the funds, or even apportion them if required.

## 6. What impact can receiving a large financial support have on the project’s community?

Questions are raised about receiving funding from a company that the community considers as "bad," in which case there could be a schism in the community - forks can be born this way.

Several examples and anecdotes were shared.

Mozilla receiving money from Google was perceived as a betrayal of their core values to an extent, and had to cut that association eventually. There is an anti-corporate sentiment in some OSS communities which marks as "betrayal" any company-based funding.

The acquisition of Github by Microsoft produced a lot of ship-jumping, to Gitlab's benefit - Microsoft despite some good showing recently still is perceived by many OSS communities as being a harmful corporation.

Conversely, Blender received notable funding from Ubisoft and Epic Games, two gaming industry heavy-weights. The licenses for professional 3D modelling suites are extremely high, and as a result there is an appetite in the industry to have an alternative that is more affordable, and putting money to the furthering of Blender makes sense for this industry, and the 3D modelling community in general, so goals remain aligned so far, and if it becomes the standard in teaching 3D, then this benefits the companies who hire graduates trained on Blender.

PyPi had an ancient code base for its servers, and to migrate the infrastructure would cost heavily; the Mozilla Foundation weighed in with a financial donation to enable hiring of consultants to help make the transition. Community seems to have not been affected, though the Mozilla Foundation has a positive image overall, which helps.

There is also a question of whether the project's development and direction can be influenced by a key funder. As such, transparency is preferred as a means to maintain visibility in the community as to the direction of the project, and continue to be an influence itself on the project.

Krita funds each major release with its own Kickstarter campaign, and has successfully delivered; in this case the goals are set up front, and the funds are crowd-sourced with tiers and pre-approved perks, which mitigates some concerns of influence - it essentially states influencibility through perks, upfront.


## 7. Would donators expect conditions as part of funding? If so what would be reasonable conditions?

The question specifically meant to ask, whether donators could expect to be able to impose conditions on the funding.

To start with, it was raised that under US jurisdiction, a donation cannot be tied to conditions, else it forms a contract to engage services and becomes a business transaction.

A donation should then be regarded as an expansion of trust, faith that the project will continue to operate within its frame of ideals, and continue to do good work, as it has done in the past. The funder pays as a recognition of past goodness, and invests in continued future goodness.

A point was raised that it could be viewed as OK to impose conditions on funding, if the conditions were in-line with the projects' goals and directions; counter-argument was that if the conditions were already in-line with the project, then they weren't really "conditions."

## 8. What level of transparency of expense would people expect?

The sentiment was that more transparency was expected of larger projects, but not so much as itemized bill expenses, but rather what sub-projects, efforts or campaigns were being spent on. For smaller projects, it was felt that transparency whilst good, was less of a concern. Of note is that if a project expresses ideals, or has as its core mission to further an ethical value, a more precise level of transparency is often expected.

Open Collective, as a method for collecting funds and using them, is focused on fully transparent accounting as the norm. Expenses are submitted openly, and paid for with funds residing with the project. Core members approve the expense, and the fiscal host pays out when funds are available.

## 9. Are there different levels of financial support expected by the size of the consumer?

If a consumer of a project is a large corporation, is it expected that their contributions should be larger?

Several examples were raised about large corporations donating small, symbolic amounts, then using a charity's or project's logo on their own page to say "we supported them." It is not easy to call out such self-serving and essentially "insulting" behaviour, as creating a polemic around a donation feels like it could be damaging to the receiver. This briefly raised the question of trademark enforcement.

In the end it seems it is the community that can express expectations, rather than the core developers/maintainers. As a potential customer, we would buy from a company that supported OSS projects properly, rather than one which was disingenuous. Here, transparency in funding sources helps make this evident.

## 10. How would FOSS projects motivate consumers to financially support them?

Examples were raised about the OpenSSL and Slackware maintainers who were found to have been severely underfunded, to a point of poverty. Upon this discovery, a massive swell of donations came forward - so one motivation seemed to be to "just ask," especially if the project is a popular one.

The corollary question was raised as to "why did they not ask sooner?" There's potentially a perception that it is presumptuous to ask for money when the maintainer sees their project as a side-project, or as simply being the enablers of a consumer's side project, or being too low down the chain. Admitting to oneself that a side project has grown to being a full time responsibility is a big mental step as well, and asking for funding forces this recognition.

Actions that maintainers can take to encourage donation then are

* simply asking for that support
* listing their request in the project's main readme, and on the front page of the project's site
* ensuring transparency about the costs of running the project
* ensuring that projects or products that incorporate your own project make clear mention of this use, so as to ensure visibility of your project is not subsumed into something else

(edit note - there's [an article that further elaborates on these points][opguide])

## 11. Would there be a need for a marketing/fundraising team to raise funds for the FOSS project?

It was noted that "marketing" was not a well-regarded concept amongst engineers. "Marketing" is something you need when you are a company. An open source project on the other hand can leverage a community to spread the word on the project, and push for the project's widespread adoption and raise its visibility. There is the prevailing notion that developers will ensure a project is "good," and that wider adoption would build a good case for funding, and that the community would do a lot of the "marketing."

As to receiving funding, especially from larger corporations, it was recognised that a specialised role would need to be created to be able to perform "business-to-business"-like functions such as negotiating funding, and addressing corporations at industry events to encourage adoption from the top level.

## 12. What other means of support can be provided if not an injection of funds?

More of a listing of ideas for the final question:

* granting server space
* donating hardware to support testing
* contributing code
* performing testing
* community moderation
* ensuring positive feedback reaches developers, not just negative outcries
* documentation writing, technical writers
* translators
* issue triagers
* administration/governance specialists
* designers
* talks/advocacy
* ensuring presence in community help and support (forums, stack overflow, topical/industry forums), providing troubleshooting assistance
* [be kind to the maintainers][george-maintainer]

[openssl-poor-funding]: https://www.zdnet.com/article/openssl-needs-corporate-funding-to-avoid-heartbleed-repeat/
[lf-grants]: https://www.linux.com/news/how-find-funding-open-source-project
[lf-cii]: https://www.linuxfoundation.org/press-release/2014/10/linux-foundations-core-infrastructure-initiative-issues-call-for-grant-proposals/
[cii-news]: https://www.coreinfrastructure.org
[slackware-patreon]: https://www.patreon.com/slackwarelinux/overview
[slackware-patreon-forum]: https://www.linuxquestions.org/questions/slackware-14/is-this-patreon-account-legit-4175658493/page4.html#post6021503
[opguide]: https://opensource.guide/getting-paid/
[codesponsor]: https://hackernoon.com/why-funding-open-source-is-hard-652b7055569d
[opencollective]: http://opencollective.com/
[bountysource]: https://www.bountysource.com/
[githubsponsors]: https://github.com/sponsors
[mint-clem]: https://blog.linuxmint.com/?p=3736
[george-maintainer]: https://feaneron.com/2019/03/28/on-being-a-free-software-maintainer/

