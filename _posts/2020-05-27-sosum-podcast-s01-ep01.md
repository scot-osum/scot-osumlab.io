---
layout: post
title: Ep01 - Hackerspaces in Northern Scotland with Tom Jones
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcast/2020/05/27/sosum-podcast-s01-ep01.html
podcast_title: Ep01 - Hackerspaces in Northern Scotland with Tom Jones
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/download/episode/28478521/sosum_s01_ep01_recording_ready.mp3"
podcast_guid: 28478521
podcast_length: 22543854
podcast_author: Ashley Nicolson
podcast_keywords: technology,scotland,opensource,hackerspace
podcast_duration: 1788
podcast_description: Super excited to welcome everyone to Scotland Open Source first episode! We have the amazing pleasure of speaking to Tom Jones, an extremely successful tech and hackerspace organiser and FreeBSD contributor. We chat about our experiences with open source in education, hackerspaces in Aberdeen, fantastic advice on organising meetups, campGND, FreeBSD and Google Summer of Code.
podcast_subtitle: Super excited to welcome everyone to Scotland Open Source first episode! We have the amazing pleasure of speaking to Tom Jones, an extremely successful tech and hackerspace organiser and FreeBSD contributor...
podcast_summary: Super excited to welcome everyone to Scotland Open Source first episode! We have the amazing pleasure of speaking to Tom Jones, an extremely successful tech and hackerspace organiser and FreeBSD contributor. We chat about our experiences with open source in education, hackerspaces in Aberdeen, fantastic advice on organising meetups, campGND, FreeBSD and Google Summer of Code.
---

Super excited to welcome everyone to Scotland Open Source first episode! We have the amazing pleasure of speaking to Tom Jones, an extremely successful tech and hackerspace organiser and FreeBSD contributor. We chat about our experiences with open source in education, hackerspaces in Aberdeen, fantastic advice on organising meetups, campGND, FreeBSD and Google Summer of Code.

<a class="spreaker-player" href="https://www.spreaker.com/episode/28478521" data-resource="episode_id=28478521" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep01: Hackerspaces in Northern Scotland with Tom Jones" on Spreaker.</a>

## Shownotes

* 1:37 - How did you begin your journey in open source? And what about it appealed to you? 
* 3:07 - What was your experiences with open source and education when you attended Uni? 
* 6:37 - How did that lead you into 57North hacklab as one of the founders?
* 9:14 - What contributed to its (57North hacklab) success?
* 11:42 - The distribution of open source communities in Scotland especially in Aberdeen - NorthernRST
* 13:43 - What is the key aspect keeping a meetup or a conference sustainable?
* 18:15 - Striving for open source within your technology stack. How would you encourage others to reach this?
* 20:55 - Google Summer of Code & FreeBSD
* 25:55 - campGND 29th - 31st May 2020

## Resources

* [campGND](https://campgnd.com/)
* [NorthernRST](https://northernrst.com/)
* [57North Hacklab](https://57north.co/)
* [Twitch: @tjhacking](https://www.twitch.tv/tjhacking/)
* [Website: https://adventurist.me/](https://adventurist.me/)
* [Mastodon: @_tj@mastodon.social](https://mastodon.social/@_tj)
* [Twitter: adventureloop](https://twitter.com/adventureloop)
* [FreeBSD](https://www.freebsd.org/)
* [Google Summer of Code](https://summerofcode.withgoogle.com/)

## Transcript

Coming Soon!