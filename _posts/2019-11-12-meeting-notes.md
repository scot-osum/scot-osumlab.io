---
layout: post
title: Open source licenses and enterprise adoption
author: Tai Kedzierski
categories: blog
---

There are a number of different licenses available in open source, and all of those approved by the Open Source Initiative allow a base set of rights - but some have additional requirements on top of them, requiring still a firm understanding of licensing issues.

Most notably, there is the difference between so-called "permissive licenses," and firmer "copyleft" ones. The existence of copyleft notably causes concerns for companies who base their business model on a "secret sauce" method.

This session is intended to cover some of the better known licenses and their requirements (and highlight things that are not required), and then open up to discussion around their various effetcs on adoption in the commercial space. We will not cover in the license overview any licenses not endorsed by the OSI, but the discussion can approach any license we wish to discuss, OSI or not.

## The Open Source Initiative

The OSI documents, approves and maintains lists of licenses that adhere to the "open source definition."

Essentially, if something is "open source" then the following are true:

* the source is available
* the software can be used by anybody, for any purpose
* the software's license must not impose restrictions/requirements on co-distributed software

There are other points as well, see the full Open Source Definition: <https://opensource.org/osd>

Note that this is not "copylefting." Software under MIT license when not distributed with corresponding source is not open-source, even if based on open source code.

## Github

Github posted in [2015 an overview of license distribution][usage-post] on their platform

Rank| 	License| 	% of projects
----|-----------|-----------------
1| 	MIT| 	44.69%
2|	Other|	15.68%
3|	GPLv2|	12.96%
4|	Apache|	11.19%
5|	GPLv3|	8.88%
6|	BSD 3-clause|	4.53%
7|	Unlicense|	1.87%
8|	BSD 2-clause|	1.70%
9|	LGPLv3|	1.30%
10| 	AGPLv3|	1.05%

![](/assets/license_trend_2015_github.png)

[usage-post]: https://github.blog/2015-03-09-open-source-license-usage-on-github-com/

## Code with no stated license

If code does not have a license, using it may pose legal problems further down the line.

Copyright law by default asserts that from the moment of publication, all rights are reserved to the author. It is up to the author to explicitate any other terms, by way of stating the rights granted. Often this is in the form of a license declaration file or paragraph.

This is different from the [Unlicense(.org)][unlicenseorg], which is a public-domaining license.

[unlicenseorg]: https://unlicense.org/

## Permissive Licenses

> In putting together these notes, I used TLDR Legal's notes, a website that summarises licenses. None of this is legal adivce.

Permissive licenses basically allow anybody to use the software for any purpose, so long as the original authorship notice is retained where it is integrated into another project, or re-distributed. Typically, they also waive any responsibility for the functioning of the software for any purpose (like any other piece of software then).

### MIT Expat and BSD 2-clause licenses

The MIT Expat license makes no demands beyond the basics. You can even incorporate the code into a project that makes any arbitrary restrictions on use. The BSD 2-clause license is similar to the MIT license

### BSD 3-clause and Apache licenses

The BSD 3-clause specifically adds a no-trademark-usage clause. Since trademark law is different from copyright law, this simply clarifies an existing distinction.

### CC-0 - Creative Commons Public Domain license

There is no such thing as a general, implicit "public domain" license. The Creative Commons project created the CC-0 ("CC Zero") to allow people to have a license similar to public domain. It was written with multimedia use in mind, rather than software source code.

It is even more permissive than MIT and BSD licenses in that it effectively does not even require any recognition to the original authors be placed anywhere, and stands in as a more acceptable alternative to the WTFPL.

Compare also the [Unlicense][unlicenseorg].

## Copyleft Licenses

> In putting together these notes, I used the [GNU.org's FAQ notes][gnu-faq]. None of this is legal advice.

[gnu-faq]: https://www.gnu.org/licenses/gpl-faq.en.html

Copyleft licenses are deeply tied to the FSF's concept of the Four Freedoms (starting with Freedom Zero, as behooves C practitioners and their language inheritants!). Recipients of copylefted software are meant to be guaranteed these freedoms.

These freedoms are

* 0: To use the software for whatever purpose
* 1: To modify the software as the user desires
* 2: To distribute the software to anybody
* 3: To distribute the changes and the changed software for the benefit of all

Licenses that espouse this are typically curated by the Free Software Foundation, under the GNU licenses.

* GPL v2.1
* LGPL v2.1
* GPL v3
* LGPL v3
* AGPL

### GNU General Public License v2.1 & v3

Full-GPL licenses are copyleft - that is, if you integrate code or explicitly link to code that is under GPL, in a way that your code and the GPL'd code constitute a single program as-distributed, then your code must grant and perpetuate the same rights as the GPL'd code.

If you use a program to produce or process code in any way, but do not use the program's code in your deliverable, the GPL does not apply.

The version 3 of the license was created to counter manufacturers' practice of preventing the running of modified software on the devices they sell ("Tivoization," as it is called by the FSF).

### GNU Lesser GPL v2.1 & v3

LGPL licenses are more lenient - they only cover the development of libraries. If you modify the library in the context of your application, any changes to the library must be under LGPL, but the wider program itself needn't be.

### GNU Affero GPL

AGPL on the other hand is more stringent than the GPL - if your software is interacted with at all over a network, you must be able to provide the source code to whomever is legitimately allowed to access it.

## Compliance with copyleft licenses

To comply with a license there are two popular solutions:

* Actually do what the license tells you to
* Stop using the code that is under that license

So if GPL code is integrated into an incompatible code base (proprietary, or other incompatible OSI license, either way) there are two remedies:

* Actively relicense the product under GPL and release the source code
* Or just stop using the GPL portion, and cease distributing the versions that do have the GPL code in it

Note: the GPL terms apply when including GPL code into your codebase. *It has no bearing on simply using the software.*

### Compliance, not money

This follows from the Free Software Foundation's enforcement guidelines:

> * Our primary goal in GPL enforcement is to bring about GPL compliance
> * Legal action is a last resort. Compliance actions are primarily education and assistance processes to aid those who are not following the license.
> * Confidentiality can increase receptiveness and responsiveness.
>   * (...) in compliance work, initiating and continuing discussions in private demonstrates good faith, provides an opportunity to teach compliance without fear of public reprisal, and offers a chance to fix honest mistakes.
> * Community-oriented enforcement must never prioritize financial gain.

There are some additional points as well - see <https://www.fsf.org/licensing/enforcement-principles>

### A "viral" GPL

Note that if code that came in under GPL would cause your whole project to be subject to the GPL in principle, the reverse act of removing the GPL code from the project returns it to being "whatever you want."

## Questions

Questions for the audience

* Have you ever explicitly not used software because of its license?
* Has your company explicitly prevented use of open source software? Your management chain? How extensively?
* Does your management chain seem to have a favourable or unfavourable view of open source?
* Is it preferable to license under a permissive license, or under a copyleft license?
* By what means of workflows do you combine libraries and software under different licenses?
* Do you see use cases in your own projects (not the company you work for) for not releasing your software under an open source license?

Questions that would require a lawyer

* If I write plugins or mods for a program that is under a GPL license, does that constitute "linking to form a single program"?
* What if software I did not intend to release is received by another party? Do they have the right to request the source code?

# Resources

* The Open Source Definition: <https://opensource.org/osd>
* TLDR Legal: <https://tldrlegal.com/>
* GNU FAQ <https://www.gnu.org/licenses/gpl-faq.en.html>
	- GNU Compatibility table: <https://www.gnu.org/licenses/gpl-faq.en.html#AllCompatibility>
	- General compatibility notes: <https://www.gnu.org/licenses/license-compatibility.html>
* Copyleft Comprehensive Guide <https://copyleft.org/guide/comprehensive-gpl-guide.html>
* FSF Enforcement Principles: <https://www.fsf.org/licensing/enforcement-principles>
