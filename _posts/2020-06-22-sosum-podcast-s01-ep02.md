---
layout: post
title: Ep02 - Ensuring longevity after unfortunate circumstances in OSS Projects with Gary Ewan Park
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcasts/2020/06/22/sosum-podcast-s01-ep02.html
podcast_title: Ep02 - Ensuring longevity after unfortunate circumstances in OSS Projects with Gary Ewan Park
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/v2/episodes/32441042/download.mp3"
podcast_guid: 32441042
podcast_length: 28567658
podcast_author: Ashley Nicolson
podcast_keywords: technology,scotland,opensource
podcast_duration: 2178
podcast_description: It's a not a topic that us open source maintainers and contributors want to think about too often. What if I get hit by a bus tomorrow? What happens to my code and all the users that utilise it? In this episode we invite Gary Ewan Park, a senior software developer from the core team at the Chocolatey project, to talk about his experiences when the unfortunate situation of one of their package maintainers had sadly passed away. How the project had to approach this difficult situation, leading to the project to place mechanisms and structure in place to promote and ensure the longevity of their package maintainers code. 
podcast_subtitle: In this episode we invite Gary Ewan Park, a senior software developer from the core team at the Chocolatey project, to talk about his experiences when the unfortunate situation of when one of their package maintainers had sadly passed away...
podcast_summary: It's a not a topic that us open source maintainers and contributors want to think about too often. What if I get hit by a bus tomorrow? What happens to my code and all the users that utilise it? In this episode we invite Gary Ewan Park, a senior software developer from the core team at the Chocolatey project, to talk about his experiences when the unfortunate situation of one of their package maintainers had sadly passed away. How the project had to approach this difficult situation, leading to the project to place mechanisms and structure in place to promote and ensure the longevity of their package maintainers code.
---

It's a not a topic that us open source maintainers and contributors want to think about too often. What if I get hit by a bus tomorrow? What happens to my code and all the users that utilise it? 
In this episode we invite Gary Ewan Park, a senior software developer from the core team at the Chocolatey project, to talk about his experiences when the unfortunate situation of one of their package maintainers had sadly passed away. How the project had to approach this difficult situation, leading to the project to place mechanisms and structure in place to promote and ensure the longevity of their package maintainers code. We also talk about the expectation of inactivity with FOSS contributors, including an experience within the Cake community, which Ewan is also part of the core team, and how teaching contributors about these aspects as soon as possible and means of seeing if a package is potentially abandoned. 

<a class="spreaker-player" href="https://www.spreaker.com/episode/32441042" data-resource="episode_id=32441042" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep02 - Ensuring longevity after unfortunate circumstances in OSS Projects with Gary Ewan Park" on Spreaker.</a>

## Shownotes

* 1:02 - How did you start your journey into open source?
* 2:35 - How did become from being a Chocolatey community member to a full time employee? 
* 4:50 - Do you find there are many job opportunities for Scottish open source developers?
* 6:40 - How are you handling the current lockdown working remotely for an OSS project as well as being a parent of young kids?
* 7:49 - Do you find COVID-19 has affected other of the contributors in terms of an increase or decrease in activity?
* 9:02 - Its hard to see the life of either a contributor or maintainer and how situations affect them causing slow down in activity or a complete lack of. Have you experience that in your own projects?
* 11:44 - The maintainer of an add-on within the Cake project has sadly passed away and they had all sole ownership of it.
* 13:40 - Unable to take over the package on GitHub due to their policy, Nuget has also updated their Deceased User policy to require evidence of death.
* 16:40 - How did the maintainers of the community feel about the new structure of the project and requesting they joined the project’s repository organisation to allow access to their code.
* 19:41 - Is there any plan to share that GitHub feature to the other members of the Community? 
* 20:30 - Do you have any visibility on a contributor’s increase or decrease in activity to anticipate that you may need to reach out and see if they need any help?
* 23:12 - One particular package maintainer disappeared story, they got married.
* 24:50 - FOSS projects often don’t have documentation on how to handle these types of situations (death, inactivity, abandonment). How did your project learn to document and act upon this situation when it arise?
* 27:50 - There is a mechanisms in NuGet to help Cake project related packages highlight new contributors and then reach out and discuss immediately the longevity of the code.
* 30:14 - Do you have any thoughts or ideas, whether it’s tools or documentation, to help projects consider and prepare how they should handle these situations?
* 33:30 - Being proactive and reactive to situations. How inactivity has an affect on the project itself it’s users.
* 34:41 - Thank you for joining! Where can people find you?

## Resources

* [Website: Gary Ewan Park](https://www.gep13.co.uk/)
* [Twitter: @gep13](https://twitter.com/gep13)
* [Twitch: @gep13](https://www.twitch.tv/gep13)
* [Chocolatey](https://chocolatey.org/)
* [Twitter: @Chocolatey](https://twitter.com/chocolateynuget)
* [Nuget](https://www.nuget.org/)
* [Cake Build](https://cakebuild.net/)
* [Twitter: @CakeBuild](https://twitter.com/cakebuildnet)
* [Twitter: @CakeContrib](https://twitter.com/cakecontrib)
* [GitHub Deceased User Policy](https://help.github.com/en/github/site-policy/github-deceased-user-policy)
* [GitHub Successor feature](https://help.github.com/en/github/setting-up-and-managing-your-github-user-account/maintaining-ownership-continuity-of-your-user-accounts-repositories)

## Transcript

Coming Soon!
