---
layout: post
title: Ep04 - KDE & the future of apps, Open Development applied to non tech communities & OpenUK with Jonathan Riddell 
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcasts/2020/08/11/sosum-podcast-s01-ep04.html
podcast_title: Ep04 - KDE & the future of apps, Open Development applied to non tech communities & OpenUK with Jonathan Riddell
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/v2/episodes/40194061/download.mp3"
podcast_guid: 40194061
podcast_length: 39400000
podcast_author: Scotland Open Source
podcast_keywords: technology,scotland,opensource,software
podcast_duration: "41:54"
podcast_description: In this episode we talk to a well know figure in Scottish open source and influencer Jonathan Riddell, the creator of Kubuntu and KDE Neon (among other amazing open source contributions). We talk about his vision for KDE Neon and the KDE community by bringing open source apps to a wider audience through proprietary app platforms. We also discuss how we can apply the principles of open development to offline communities as well as the Scottish government and why we think its needed. Lastly we highlight the events that OpenUK, which Jonathan is the chair of the awards committee, are planning and their aspirations to promote open tech in the UK.
podcast_subtitle: In this episode we talk to a well know figure in Scottish open source and influencer Jonathan Riddell, the creator of Kubuntu and KDE Neon (among other amazing open source contributions)...
podcast_summary: In this episode we talk to a well know figure in Scottish open source and influencer Jonathan Riddell, the creator of Kubuntu and KDE Neon (among other amazing open source contributions). We talk about his vision for KDE Neon and the KDE community by bringing open source apps to a wider audience through proprietary app platforms. We also discuss how we can apply the principles of open development to offline communities as well as the Scottish government and why we think its needed. Lastly we highlight the events that OpenUK, which Jonathan is the chair of the awards committee, are planning and their aspirations to promote open tech in the UK.
---

Being in open source has many wonderful opportunities, including traveling the world and meeting some inspirational people. One of its biggest appeals is that we can see it changing the world and can impact those that use it for the better...

..

In this episode we talk to a well know figure in Scottish open source and influencer Jonathan Riddell, the creator of Kubuntu and KDE Neon (among other amazing open source contributions). We talk about his vision for KDE Neon and the KDE community by bringing open source apps to a wider audience through proprietary app platforms. We also discuss how we can apply the principles of open development to offline communities as well as the Scottish government and why we think its needed. Lastly we highlight the events that OpenUK, which Jonathan is the chair of the awards committee, are planning and their aspirations to promote open tech in the UK.

<a class="spreaker-player" href="https://www.spreaker.com/episode/40194061" data-resource="episode_id=40194061" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep04 - KDE &amp; the future of apps, Open Development applied to non tech communities &amp; OpenUK with Jonathan Riddell" on Spreaker.</a>


## Shownotes

* 1:23 - How did you start your journey into open source and what about it appealed to you?
* 2:55 - Did you always have that mindset that open source can improve the world?
* 3:43 - IBM talk that claimed that Linux desktop will take over the world which led into the KDE project
* 5:08 - Q: How did you get involved with Ubuntu? A: Through the Quaker's free software network.
* 6:21 - "Super secret start up"
* 7:38 - How did Kubuntu start off and now KDE Neon and what makes it different to other traditional distros (in the face that it isn't a distro at all)
* 10:06 - KDE slightly pigeon holed
* 10:47 - Ubuntu releases every 6 months
* 11:40 - Found current Linux distros limiting and waiting to change that with KDE Neon
* 14:04 - What is your vision with the KDE community and making KDE Neon more accessible to a wider audience
* 15:40 - Using latest package management like snap packages etc
* 16:00 - Building apps that can be shared across multiple distros
* 16:43 - Is this in progress just now? Yes, but it's a culture shift.
* 17:20 - Who maintains these apps?
* 19:09 - You said that creating new apps is a culture shirt, what are the problems you are seeing to encourage this?
* 22:50 - What other sort of benefits to our open source project do you think having its apps on proprietary app stores?
* 23:30 - KDE Academy 2020 (online 4th-11th September)
* 25:09 - What common traits from your open development have you applied to non-technical communities you are a part of?
* 27:05 - Where's the Water project
* 28:30 - Did this community have to learn how to contribute using new tools, how was their experience?
* 29:45 - How do you think the aspects of open development could be applied to our Scottish institudes, governments or other industries?
* 31:05 - Visited Munich and Barcelona governments that have embraced open source
* 33:13 - That hasn't happened in Scotland yet...
* 34:18 - We need a culture shift to focus on in-house technical knowledge instead of outsourcing.
* 35:06 - Do you think that Scotland is missing the FOSS consultancies?
* 35:50 - OpenUK - Any upcoming events?
* 38:50 - As a long time contributor to FOSS what advice would you give to contributors to educate themselves on?
* 40:01 - Umbrello still being used today...
* 40:33 - How can people reach you?

## Resources

* [Website: Jonathan Riddell](https://jriddell.org)
* [Twitter: @jriddell](https://twitter.com/jriddell)
* [Blue Systems: https://www.blue-systems.com/](https://www.blue-systems.com/)
* [Krita: https://krita.org/en/](https://krita.org/en/)
* [F-Droid: https://f-droid.org/](https://f-droid.org/)
* [KDE Academy: https://akademy.kde.org/](https://akademy.kde.org/)
* [Where's the Water: https://www.canoescotland.org/go-paddling/wheres-the-water](https://www.canoescotland.org/go-paddling/wheres-the-water#gsc.tab=0)
* [OpenUK: https://openuk.uk/](https://openuk.uk/)
* [MimuGloves: https://mimugloves.com/](https://mimugloves.com/)
* [OpenUK Giveaway: Kids Competition with Digital Summer Camp ](https://openuk.uk/openuk-announces-expansion-of-kids-competition-with-digital-summer-camp-3000-minimu-glove-kit-giveaway/)
* [Umbrello](https://umbrello.kde.org/)
* [Matrix](https://matrix.org/)

## Transcript

* [Transcript (Coming Soon)]()

## Contribute

If you see anything to adjust, spelling or context, please feel free to contribute on the [Scotland Open Source Users Meetups's Repo](https://gitlab.com/scot-osum/scot-osum.gitlab.io/)

