---
layout: post
title: Ep03 - Exploitation of FOSS developers, Education and PR to help projects with Greg Sutcliffe
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcasts/2020/07/07/sosum-podcast-s01-ep03.html
podcast_title: Ep03 - Exploitation of FOSS developers, Education and PR to help projects with Greg Sutcliffe
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/v2/episodes/37377465/download.mp3"
podcast_guid: 37377465
podcast_length: 30909303
podcast_author: Scotland Open Source
podcast_keywords: technology,scotland,opensource,software
podcast_duration: "37:23"
podcast_description: In this episode we talk to long time open source veteran Greg Sutcliffe, a community data scientist and part of the community team on the Ansible project. We explore the exploitations of open source developers and working for exposure is something we should not encourage. We discuss and contemplate how to encourage companies and government funded institutes to adopt open source from in-house developers, specific PR efforts and education on open source business models. 
podcast_subtitle: In this episode we talk to long time open source veteran Greg Sutcliffe, a community data scientist and part of the community team on the Ansible project. We explore the exploitations of open source developers...
podcast_summary: In this episode we talk to long time open source veteran Greg Sutcliffe, a community data scientist and part of the community team on the Ansible project. We explore the exploitations of open source developers and working for exposure is something we should not encourage. We discuss and contemplate how to encourage companies and government funded institutes to adopt open source from in-house developers, specific PR efforts and education on open source business models.
---

Open source developers now a days find themselves doing work for the sake of exposure because we have become accustomed to the “portfolio building” aspect of getting noticed within the community. But this isn’t how it should be...
In this episode we talk to long time open source veteran Greg Sutcliffe, a community data scientist and part of the community team on the Ansible project. We explore the exploitations of open source developers and working for exposure is something we should not encourage. We discuss and contemplate how to encourage companies and government funded institutes to adopt open source from in-house developers, specific PR efforts and education on open source business models. 

<a class="spreaker-player" href="https://www.spreaker.com/episode/37377465" data-resource="episode_id=37377465" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep03 - Exploitation of FOSS developers, Education and PR to help projects with Greg Sutcliffe" on Spreaker.</a>

## Shownotes

* 1:28 - How did you start your journey into open source and what about it applied to you?
* 2:21 - First steps in open source was because of an iPod
* 3:15 - Joined the local Linux User Group - Falkirk
* 3:47 - What made you think to give back to the community?
* 4:42 - Aha! Here is a project I can contribute to (Foreman)
* 5:40 - How did you get recruited by Redhat through your contributions to Foreman?
* 6:29 - My boss saw that he got more from me if we contributed to the project as a whole instead of building from scratch
* 7:25 - Do stuff that no one wants to do, you'll get thanks from the maintainers at least
* 8:43 - We now see open source contributors having to contribute as part of a portfolio building - "free exposure"
* 10:18 - What kind of advice would you give to both contributors or companies that want to be recruited or to recruit in the community?
* 11:41 - As a company, its a bit obvious who your good contributors are
* 12:35 - From an agency persepective embedding themselves within the community is good advice to see who to recruit
* 13:05 - Diversity in the community, specifically who contributors work for
* 14:08 - First day at RedHat
* 14:54 - What can open source technologies or communities do to help businesses that are struggling with during lockdown?
* 16:22 - Research shows that working from home is more productive
* 17:30 - Dare I say may need more marketing [for open source software]
* 18:20 - People find a hinderance of open source is the lack of support
* 19:44 - Public money, Public code
* 20:20 - Outsourcing versus In-house developers running open source platform
* 21:37 - Do we have enough open source experts to talk to companies?
* 21:55 - People who get involved in open source are not usually from PR or Marketing background 
* 22:23 - How to change this mindset? In Universities or education?
* 22:51 - Not enough open source or even technology experts
* 23:21 - Raspberry Pis helps children to learn by "breaking" and not affecting the family's PC
* 24:32 - Stirling University had a workshop of open source
* 25:42 - PR is abit of a dirty word to us techies, but it shouldn't be
* 26:14 - Some of most successful open source project had marketing teams behind the project
* 27:19 - Projects can get lost among others but PR should be able to help
* 28:35 - Who is the priority to market to?
* 30:08 - With the broad adoption then in theory it should help towards the funding situation
* 30:38 - There are other business models that companies should be educated on
* 32:53 - Have you got any current projects at the moment?
* 33:17 - R stats community, working on a NextCloud package
* 34:21 - Love to get back into stuff, extremely interested in education
* 35:14 - How can people reach you?

## Resources

* [Website: Greg Sutcliffe](https://emeraldreverie.org)
* [Twitter: @Gwmngilfen](https://twitter.com/Gwmngilfen)
* [Fediverse: @gwmngilfen@fosstodon.org](https://fosstodon.org/@gwmngilfen)
* [Ansible](https://www.ansible.com/)
* [Ansible: Community Team](https://www.ansible.com/blog/author/the-ansible-community-team)
* [RedHat: Twitter](https://twitter.com/redhat)
* [Falkirk Linux User Group:TechMeetup](https://twitter.com/techmeetup)
* [Foreman](https://www.theforeman.org/)
* [R Group (Glasgow)](https://twitter.com/teamrglasgow?lang=en)
* [GHClass](https://rundel.github.io/ghclass/)
* [Jenkins Keynote @ FOSDEM 2013](https://archive.fosdem.org/2013/schedule/event/keynote_vibrant_developer_community/)

## Transcript

* [Transcript (unformatted)](http://scot-osum.gitlab.io/transcripts/2020-07-07-sosum-podcast-s01-ep03-transcript.html)

## Contribute

If you see anything to adjust, spelling or context, please feel free to contribute on the [Scotland Open Source Users Meetups's Repo](https://gitlab.com/scot-osum/scot-osum.gitlab.io/)

