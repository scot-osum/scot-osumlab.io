---
layout: post
title: Ep06 - Supporting FOSS Maintainers, handling conflict, Homebrew & Software Freedom Conservancy with Mike McQuaid
author: Scotland Open Source Users Meetups
categories: podcasts
podcast_url: http://scot-osum.gitlab.io/podcasts/2021/02/24/sosum-podcast-s01-ep06.html
podcast_title: Ep06 - Supporting FOSS Maintainers, handling conflict, Homebrew & Software Freedom Conservancy with Mike McQuaid
podcast_owner: Scotland Open Source Users Meetups
podcast_explicit: clean
podcast_file: "https://api.spreaker.com/v2/episodes/43559287/download.mp3"
podcast_guid: 43559287
podcast_length: 49831519
podcast_author: Scotland Open Source
podcast_keywords: technology, scotland, opensource, maintainers, burnout, software
podcast_duration: "60:04"
podcast_description: On this episode we welcome Mike McQuaid, a long standing maintainer (10+ years) and project lead of Homebrew. We talk about Mike’s journey of becoming a maintainer and how he manages to sustain a healthy relationship with his project and it’s community by establishing a good work life balance and having a clear definition of a maintainer’s responsibilities. As a strong advocate for supporting maintainers Mike describes how he and Homebrew’s team of maintainers deal with the negativity of both the job and certain members of the community by the use of tools, bots and a supportive approach to day to day working with one another. We also talk about Homebrew’s progression to becoming a member project of the Software Freedom Conservancy and the benefits it brings to both the project and those involved.
podcast_subtitle: We welcome Mike McQuaid, a long standing maintainer (10+ years) and project lead of Homebrew to talk about supporting OSS maintainers and experiences of being a long term maintainer.
podcast_summary: On this episode we welcome Mike McQuaid, a long standing maintainer (10+ years) and project lead of Homebrew. We talk about Mike’s journey of becoming a maintainer and how he manages to sustain a healthy relationship with his project and it’s community by establishing a good work life balance and having a clear definition of a maintainer’s responsibilities. As a strong advocate for supporting maintainers Mike describes how he and Homebrew’s team of maintainers deal with the negativity of both the job and certain members of the community by the use of tools, bots and a supportive approach to day to day working with one another. We also talk about Homebrew’s progression to becoming a member project of the Software Freedom Conservancy and the benefits it brings to both the project and those involved.

---

On this episode we welcome Mike McQuaid, a long standing maintainer (10+ years) and project lead of Homebrew. We talk about Mike’s journey of becoming a maintainer and how he manages to sustain a healthy relationship with his project and it’s community by establishing a good work life balance and having a clear definition of a maintainer’s responsibilities. As a strong advocate for supporting maintainers Mike describes how he and Homebrew’s team of maintainers deal with the negativity of both the job and certain members of the community by the use of tools, bots and a supportive approach to day to day working with one another. We also talk about Homebrew’s progression to becoming a member project of the Software Freedom Conservancy and the benefits it brings to both the project and those involved.

...

<a class="spreaker-player" href="https://www.spreaker.com/episode/43559287" data-resource="episode_id=43559287" data-width="100%" data-height="200px" data-theme="light" data-playlist="false" data-playlist-continuous="false" data-autoplay="false" data-live-autoplay="false" data-chapters-image="true" data-episode-image-position="right" data-hide-logo="false" data-hide-likes="false" data-hide-comments="false" data-hide-sharing="false" data-hide-download="true">Listen to "Ep06 - Supporting FOSS Maintainers, Homebrew &amp; Software Freedom Conservancy with Mike McQuaid" on Spreaker.</a>


## Shownotes

* 00:11 - Introduction
* 01:21 - How did you start your journey into open source?
* 02:26 - Using Linux at University
* 03:40 - End of University releasing small projects using self-hosted git repository
* 04:02 - Internship at Wolfson Microelectronics
* 04:40 - Joined Google Summer of Code with KDE project
* 05:33 - Converted onto Mac as main development environment
* 05:54 - Start of the Homebrew project by Max Howell
* 06:18 - Joined the Homebrew project as maintainer
* 07:00 - What kept you motivated to continue being a maintainer? 10+ years is a long time.
* 08:15 - Aim for the project (Homebrew) to outlive me
* 08:43 - The people of open source (the ups & down)
* 10:15 - It's hard not to take some of the negativity personally
* 10:45 - Feedback on design decisions that have evolved over time
* 11:05 - Working with the benefit of the doubt on these design decisions as many consumers are "silent" until something is wrong
* 12:05 - Pride of a maintainer and their code - A benefit and a negative?
* 13:15 - Open to praise, but potentially also negativity
* 14:30 - Day-to-Day doing things for those that work along side you
* 15:35 - Striking a balance of needs and having friendly obligations between maintainers
* 15:50 - To grow a community of maintainers
* 17:10 - Do you feel maintainers are obligated to provide to the users due to the perception of how the industry works?
* 18:14 - But the problem is when people feel they "need" to fix
* 19:05 - Open Source licenses explicitly state that liability is waived
* 20:25 - With the increased consumption of open source means more consumers raise issues versus raising fixes
* 21:50 - Maintainers don't have a legal or moral obligation to fix all the issues raised
* 22:30 - Low tolerance of rudeness or non issues helps maintainers
* 23:30 - Own experiences of users that have "contributed" by forms of feedback
* 24:40 - Do you feel that with this increased consumption of open source had led maintainers to burnout?
* 25:10 - More difficult for single maintainers versus multiple persons projects
* 26:40 - Burnout may also be attributed to the culture you are within
* 27:28 - So much energy in your tank - I like playing computer games to relieve stress
* 28:32 - Trying to be productive during your downtime may work for some if there is enjoyment
* 30:10 - Working long hours (especially in open source) with little in return can attribute to burnout
* 30:41 - The longer you're in open source your motivation needs to be intrinsic
* 31:59 - How do you encourage users to the appropriate etiquette when dealing with Maintainers & vice versa
* 32:10 - Issues Templates to be direct
* 33:11 - Being self-aware of your good & bad traits when on boarding new maintainers
* 33:53 - Being direct often helps resolve issues quicker
* 35:53 - Haven't experienced Rude members converting to good contributors
* 37:07 - Some Maintainers may find conflict difficult. Do you think we as maintainers, have support?
* 37:57 - Worked on the Open Source Guides
* 38:20 - Join conferences and meetups to meet together from all different experiences
* 39:28 - Find friends within the Robots - automate your maintainer's workflow/interactions
* 40:09 - People respond to "Robots" differently 
* 41:21 - Use bots to avoid unnecessary confrontation
* 43:55 - Thinking about the longevity of Open Source projects - Homebrew becoming a non-profit supported by Software Freedom Conservancy
* 44:30 - Commit to not personally finance the project - purchases made as a community
* 45:40 - Started a kickstarter to fund at the beginning
* 46:37 - Thinking more on our financial independence looking for a fiscal host
* 48:50 - Now have a Project Leader Committee allowing me to step back
* 50:01 - Having electoral board members including technical lead, project lead etc.
* 51:33 - Allows more diverse personalities and reduces the chance of being stagnant
* 51:47 - Covered alot of topics and that we should aim to support our Open Source Maintainers as equally as those that contribute
* 54:25 - As a long time contributor to FOSS what advice would you give to contributors to educate themselves on?
* 54:50 - "As you get older you should do fewer things better"
* 56:24 - Starting in Open Source to "scratch your own itch"
* 57:38 - Don't give up too much of yourself. Get involved in projects in a healthy way
* 58:40 - How can people reach you?
* 59:25 - Thank you



## Resources

* [Website: mikemcquaid.com](mikemcquaid.com)
* [Twitter: @MikeMcQuaid](https://twitter.com/MikeMcQuaid)
* [GitHub: https://github.com/MikeMcQuaid](https://github.com/MikeMcQuaid)
* [Homebrew](https://brew.sh/)
* [Homebrew GitHub:](https://github.com/Homebrew)
* [Software Freedom Conservancy](https://sfconservancy.org/)
* [Balancing Dads podcast (Spotify)](https://open.spotify.com/show/07rdnm3rGWXvaJF7385fxq)


## Transcript

* [Transcript (Coming Soon)]()

## Contribute

If you see anything to adjust, spelling or context, please feel free to contribute on the [Scotland Open Source Users Meetups's Repo](https://gitlab.com/scot-osum/scot-osum.gitlab.io/)

