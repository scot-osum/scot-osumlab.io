# Scotland OSUM website

Feel free to edit any page and submit a pull request.

In partcular, if you know of any FOSS-centric meetups that actively promote Open Source and Free Software awareness, please consider adding to the [meetups list file](_data/meetups.yml) !

See [contributing.md](contributing.md) for details.

## Compiling the site

You shouldn't need to have to do this unless you are testing major changes to the site

* Setup: `sudo apt-get install ruby && gem install jekyll bundler && . ~/.bashrc`
* When in this project: `jekyll serve --watch`
    * Then visit <http://localhost:4000>
    * Make edits, and refresh the page - the site rebulds automatically on-save
