---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: About
permalink: /about/
---

The Scotland Open Source Users Meetups (OSUM) aim to bring people in Scotland together around Free and Open Source Software ("FOSS"), with a focus on helping discover projects; discuss the impact FOSS has on our industry; and explore ways to support and promote a healthy open source ecosystem.

The meetups will be a combination of talks & demonstrations about all aspects of open source and how it can be incorporated into your day to day activities. There will be topics and activities for all levels of immersion; including workshops and demos of OSS; discussions & announcements on what's going on in the world of open source (new tech, changes in licenses ...); debates on open source principles and practices; and how to support and manage online communities & projects etc.

Scotland OSUM is also keen to help raise awareness of other meetups that have a focus on discussing and advocating for Open Source and Free Software. If you are organising one for your town, do get in touch, here on on [Twitter via @ScotlandOSUM][twitter] !

WIP: We are still getting set up with the events and venues, so if you have any interesting talks/topics or you know of fellow OS advocates that would want a platform to voice how much they love open source then feel free to contact us :D

Check us out on twitter: [@ScotlandOSUM][twitter] :-)



[twitter]: https://twitter.com/ScotlandOSUM
